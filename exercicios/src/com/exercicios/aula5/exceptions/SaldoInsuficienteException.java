package com.exercicios.aula5.exceptions;

public class SaldoInsuficienteException extends RuntimeException{


	public SaldoInsuficienteException(double valor) {
		super("Você não possui saldo suficiente para efetuar o saque de $" + valor);
	}
	
	public SaldoInsuficienteException() {
		super("Você não possui saldo suficiente para efetuar o saque.");
	}
	
	public SaldoInsuficienteException(String msg) {
		super(msg);
	}
	
	
	
	
	
}
