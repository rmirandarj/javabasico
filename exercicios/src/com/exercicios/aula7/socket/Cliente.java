package com.exercicios.aula7.socket;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Cliente {
	  
	public static void main(String[] args) throws Exception {
		new Cliente("127.0.0.1", 9999).executa();
	}
	
	private String host;
    private int porta;
    private String nickname = "";

    public Cliente (String host, int porta) {
        this.host = host;
        this.porta = porta;
    }

    public void executa() throws Exception {
        Socket cliente = new Socket(this.host, this.porta);
        System.out.println("Você se conectou ao servidor pela porta: " + this.porta + "!");
        
        // lê msgs do teclado e manda pro servidor
        Scanner teclado = new Scanner(System.in);
        PrintStream saida = new PrintStream(cliente.getOutputStream());
        
        try {
		    // thread para receber mensagens do servidor
		    Recebedor r = new Recebedor(cliente.getInputStream());
		    new Thread(r).start();
		
		    this.nickname = JOptionPane.showInputDialog("Qual o seu nickname?");
		    
		    saida.println(this.nickname);
		    
		    JOptionPane.showMessageDialog(null, "Para desconectar, digite: \"SAIR\"");
		    
		    while (teclado.hasNextLine()) {
		    	String mensagem = teclado.nextLine();
		    	if(!"SAIR".equals(mensagem)) {
		    		saida.println(mensagem);
		    	}else {
		    		saida.println(mensagem);
		    		System.out.println("Conexão encerrada com sucesso!");
		    		break;
		    	}
		    }
        }
	    catch (Exception e) {
			e.printStackTrace();
		}
        finally {
        	saida.close();
		    teclado.close();
		    cliente.close();
		}
		
		            
    }

    public class Recebedor implements Runnable {

        private InputStream servidor;

        public Recebedor(InputStream servidor) {
            this.servidor = servidor;
        }

        public void run() {
            // recebe msgs do servidor e imprime na tela
            Scanner s = new Scanner(this.servidor);
            while (s.hasNextLine()) {
                System.out.println(s.nextLine());
            }
        }

    }
	
}