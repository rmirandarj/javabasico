package com.exercicios.aula7.socket;

import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.io.InputStream;
import java.util.Scanner;

/***
 * Implementar a verificação de conexões cliente fechadas com troca de mensagens, 
 * assim como implementei o fechamento de conexão do lado do cliente por mensagem. 
 * @author Raphael
 *
 */

public class Servidor {
	
	private int porta;
	private List<PrintStream> clientes;
	
	public static void main(String[] args) throws Exception {
		new Servidor(9999).executa();
	}
	
	public Servidor(int porta) {
		this.porta = porta;
        this.clientes = new ArrayList<PrintStream>();
	}
	
	public void executa() throws Exception {
		ServerSocket servidor = new ServerSocket(this.porta);
		System.out.println("Porta " + this.porta + " aberta!");

		while (true) {
			
			Socket cliente = servidor.accept();
			String nickname = new Scanner(cliente.getInputStream()).nextLine();
			System.out.println("Nova conexão com o cliente " + nickname);
			
			// adiciona saida do cliente à lista
            PrintStream ps = new PrintStream(cliente.getOutputStream());
            this.clientes.add(ps);
			
           
			TrataCliente tc = new TrataCliente(cliente.getInputStream(), ps, nickname, this);
            new Thread(tc).start();
            
 
		}
	}
	
	public void distribuiMensagem(String msg) {
        // envia msg para todo mundo
        for (PrintStream cliente : this.clientes) {
            cliente.println(msg);
        }
    }
	
	public class TrataCliente implements Runnable {

	    private InputStream inputStreamCliente;
	    private PrintStream psCliente;
	    private String nicknameCliente;
	    private Servidor servidor;

	    public TrataCliente(InputStream inputStreamCliente, PrintStream ps, String nicknameCliente, Servidor servidor) {
	        this.inputStreamCliente = inputStreamCliente;
	        this.psCliente = ps;
	        this.nicknameCliente = nicknameCliente;
	        this.servidor = servidor;
	    }

	    public void run() {
	        Scanner s = new Scanner(this.inputStreamCliente);
	        while (s.hasNextLine()) {
	        	
	        	String mensagem = s.nextLine().trim();
	        	if(!"SAIR".equals(mensagem)) {
	        		servidor.distribuiMensagem(this.nicknameCliente + " diz: " + mensagem);
					System.out.println(s.nextLine());
	        	}else {
	        		servidor.clientes.remove(this.psCliente);
	        		servidor.distribuiMensagem(this.nicknameCliente + " saiu");
	        		break;
	        	}
	        	
	            
	        }
	        s.close();
	    }

	}
}