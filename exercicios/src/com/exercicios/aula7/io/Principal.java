package com.exercicios.aula7.io;

import java.io.FileNotFoundException;

public class Principal {

	public static void main(String[] args) {
		try {
			
			Corrida corrida = LeArquivo.retornaCorridaDoArquivo();
			
			//1- A)
			System.out.println(corrida.getMelhorTempoDaCorrida());
			
			//1- B)
			System.out.println(corrida.getMelhoresTempos());
			
			//1- C)
			System.out.println(corrida.getPioresTempos());
			
			//1- D)
			System.out.println(corrida.getMediasDosTemposDeCadaPiloto());
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}

	}

}
