package com.exercicios.aula7.io;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class DadosPiloto {
	
	private String nomePiloto;
	private List<Double> tempos;
	
	public DadosPiloto(String nomePiloto, List<Double> tempos) {
		super();
		this.nomePiloto = nomePiloto;
		this.tempos = tempos;
	}

	public String getNomePiloto() {
		return nomePiloto;
	}

	public List<Double> getTempos() {
		return tempos;
	}
	
	public Double getMelhorTempo() {
		
		Double melhorTempo = Double.MAX_VALUE;
		
		for (Double t : tempos) {
			if(t < melhorTempo)		melhorTempo = t;
		}
		
		return melhorTempo;
	}
	
	public Double getPiorTempo() {
		
		Double piorTempo = Double.MIN_VALUE;
		
		for (Double t : tempos) {
			if(t > piorTempo)		piorTempo = t;
		}
		
		return piorTempo;
	}
	
	public Double getMediaTempos() {
		
		double somatorio = 0.0;
		int numVoltas = 0;
		
		for (Double t : tempos) {
			somatorio += t.doubleValue();
			numVoltas++;
		}
		
		return somatorio / numVoltas;
	}
	
	
	public String getDadosMelhorTempo() {
		
		Double melhorTempo = Double.MAX_VALUE;
		int numVolta = 0;
		String piloto = "";
		
		double tempoAtual = this.getMelhorTempo();
		
		if(tempoAtual < melhorTempo) {
			melhorTempo = tempoAtual;
			
			numVolta = this.getTempos().indexOf(tempoAtual) + 1;
			piloto = this.getNomePiloto();
		}
		
		return "\nMelhor tempo do " + piloto + ": " + melhorTempo +
				"\nVolta : " + numVolta;
	}
	
	public String getDadosPiorTempo() {
		
		int numVolta = 0;
		String piloto = "";
		
		double piorTempo = this.getPiorTempo();
		
		numVolta = this.getTempos().indexOf(piorTempo) + 1;
		piloto = this.getNomePiloto();
		
		return "\nPior tempo do " + piloto + ": " + piorTempo +
				"\nVolta : " + numVolta;
	}
	
	public String getDadosMediaTempo() {
		
		int numVolta = 0;
		String piloto = "";
		
		double media = this.getMediaTempos();
		piloto = this.getNomePiloto();
		
		return "\nMédia de tempo do " + piloto + ": "+ String.format(Locale.US, "%.3f", media);
	}
	
	
}
