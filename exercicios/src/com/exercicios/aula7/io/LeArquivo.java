package com.exercicios.aula7.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class LeArquivo {
	
	public static Corrida retornaCorridaDoArquivo() throws FileNotFoundException{
		Scanner scanner = new Scanner(new FileReader("tempos.txt"));
		
		scanner.useDelimiter("\n");
		Corrida corrida = null;
		if (scanner.hasNext()) {
			//===
			scanner.next();
			
			int numPilotos = Integer.parseInt(scanner.next().split(": ")[1].trim());
			int numVoltas = Integer.parseInt(scanner.next().split(": ")[1].trim());
			
			corrida = new Corrida(numPilotos, numVoltas, new ArrayList<DadosPiloto>());
			
			String nomePiloto;
			List<Double> tempos;
			DadosPiloto dadosPiloto;
			
			for(int i=0; i < numPilotos; i++) {
				
				nomePiloto = scanner.next();
				tempos = new ArrayList<Double>();
				
				for(int j=0; j < numVoltas; j++) {
					tempos.add(scanner.nextDouble() / 1000);
				}
				
				dadosPiloto = new DadosPiloto(nomePiloto, tempos);
				corrida.addDadosPiloto(dadosPiloto);
			}
		}
		
		scanner.close();
		
		return corrida;
	}
}
