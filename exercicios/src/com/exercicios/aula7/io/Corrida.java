package com.exercicios.aula7.io;

import java.util.Collection;
import java.util.List;

public class Corrida {
	
	private int numPilotos;
	private int numVoltas;
	private Collection<DadosPiloto> dadosPilotos;
	
	public Corrida(int numPilotos, int numVoltas, Collection<DadosPiloto> dadosPilotos) {
		super();
		this.numPilotos = numPilotos;
		this.numVoltas = numVoltas;
		this.dadosPilotos = dadosPilotos;
	}

	public int getNumPilotos() {
		return numPilotos;
	}

	public int getNumVoltas() {
		return numVoltas;
	}

	public boolean addDadosPiloto(DadosPiloto dp) {
		return this.dadosPilotos.add(dp);
	}
	
	public String getMelhorTempoDaCorrida() {
		
		Double melhorTempo = Double.MAX_VALUE;
		int numVolta = 0;
		String melhorPiloto = "";
		
		for (DadosPiloto dp : dadosPilotos) {
			double tempoAtual = dp.getMelhorTempo();
			
			if(tempoAtual < melhorTempo) {
				melhorTempo = tempoAtual;
				
				numVolta = dp.getTempos().indexOf(tempoAtual) + 1;
				melhorPiloto = dp.getNomePiloto();
			}
			
		}
		
		return "\nMelhor tempo da prova: " + melhorTempo +
				"\nVolta : " + numVolta +
				"\nPiloto: " + melhorPiloto;
	}
	
	public String getMelhoresTempos() {
		
		StringBuilder dados = new StringBuilder();
		
		for (DadosPiloto dp : this.dadosPilotos) {
			dados.append(dp.getDadosMelhorTempo());
		}
		
		return dados.toString();
	}
	
	public String getPioresTempos() {
		
		StringBuilder dados = new StringBuilder();
		
		for (DadosPiloto dp : this.dadosPilotos) {
			dados.append(dp.getDadosPiorTempo());
		}
		
		return dados.toString();
	}
	
	public String getMediasDosTemposDeCadaPiloto() {
		
		StringBuilder dados = new StringBuilder();
		
		for (DadosPiloto dp : this.dadosPilotos) {
			dados.append(dp.getDadosMediaTempo());
		}
		
		return dados.toString();
	}
}
