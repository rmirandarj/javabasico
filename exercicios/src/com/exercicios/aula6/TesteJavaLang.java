package com.exercicios.aula6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TesteJavaLang {
	public static void main(String[] args) {
		
		//Aula 6 - exercicio 1
		List<Conta> listaContas = new ArrayList<Conta>(); 
		Conta.adicionarContasAleatorias(listaContas, 10);
		
		for (Conta conta : listaContas) {
			System.out.println(conta);
		}
		
		System.out.println("------------------------------------------------------------------------------------");
		
		//Aula 6 - exercicio 2
		Set<Conta> setContas = new HashSet<Conta>(); 
		setContas.add(new Conta(10, 11, "Whatever", 10000));
		setContas.add(new Conta(10, 11, "Whatever", 10000));
		
		for (Conta conta : setContas) {
			System.out.println(conta);
		}
		
		System.out.println("------------------------------------------------------------------------------------");
		
		//Aula 6 - exercicio 3
		String s = "fj11";
		s.replaceAll("1", "2");
		System.out.println(s);
		
		//retornando uma nova String
		System.out.println(s.replaceAll("1", "2"));
		
		//Aula 6 - exercicio 4
		String str1 = "casamento";
		String str2 = "casa";
		
		System.out.print("\n");
		
		System.out.println(str2 + " é substring de " + str1 + "?");
		System.out.println(str1.indexOf(str2) < 0 ? "Não!" : "Sim!!!");
		
		System.out.print("\n");
		
		String strComEspacos = " teste ";
		System.out.println("Sem espaços:" +strComEspacos.trim());
		
		System.out.print("\n");
		
		String strVazia = "";
		System.out.println(strVazia.isEmpty());
	}
}
