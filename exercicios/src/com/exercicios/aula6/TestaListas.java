package com.exercicios.aula6;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.exercicios.aula6.Conta;

public class TestaListas {
	public static void main(String[] args) {

		List<Conta> listaContas = new ArrayList<Conta>(); 
		Conta.adicionarContasAleatorias(listaContas, 10);
		
		Collections.shuffle(listaContas);
		
		for (Conta conta : listaContas) {
			System.out.println(conta.mostrarDados());
		}
		
		System.out.print("\n");
		
		Collections.sort(listaContas);
		
		
		
		for (Conta conta : listaContas) {
			System.out.println(conta.mostrarDados());
		}
		
		System.out.print("\n");
		
		Collections.reverse(listaContas);
		
		
		for (Conta conta : listaContas) {
			System.out.println(conta.mostrarDados());
		}
		
		
		Banco banco = new Banco();
		Conta c1 = new Conta(1, 11, "Titular1", 100);
		Conta c2 = new Conta(1, 11, "Titular1", 100);
		
		banco.adicionaConta(c1);
		banco.adicionaConta(c2);
		
		System.out.println(banco.quantidadeDeContas());
		
	}
	
	
}
