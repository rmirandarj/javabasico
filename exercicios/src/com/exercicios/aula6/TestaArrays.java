package com.exercicios.aula6;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.exercicios.aula4.model.ContaCorrente;
import com.exercicios.aula4.model.ContaPoupanca;
import com.exercicios.aula6.Conta;

public class TestaArrays {
	
	public static void main(String[] args) {
		Conta[] contas = new Conta[10];

		criarContas(contas);
		
		System.out.println(calculaMediaDosSaldosDasContas(contas));
		
		System.out.println(inverteFrasePorPalavras("Socorram-me, subi no ônibus em Marrocos"));
		
		System.out.print("\n");
		
	}
	
	public static void criarContas(Conta[] arrayContas) {
		
		for (int i = 0; i < arrayContas.length; i++) {
			Conta conta = new Conta(i, i * 10000, "Titular" + (9 - i), (i+1) * 100);
			arrayContas[i] = conta;
		}
		
	}
	
	public static double calculaMediaDosSaldosDasContas(Conta[] arrayContas) {
		
		double totalSaldo = 0;
		int numeroContasExistentes = 0;
		
		for (Conta contaBancaria : arrayContas) {

			if(contaBancaria != null) {
				numeroContasExistentes++;
				totalSaldo += contaBancaria.getSaldo();
			}
		}
		
		return numeroContasExistentes > 0 ? totalSaldo / numeroContasExistentes : 0.0;
	}
	
	public static String inverteFrasePorPalavras(String frase) {
		String[] cortes = frase.split(" ");
		
		StringBuilder fraseInvertida = new StringBuilder();
		for (int i = cortes.length-1; i >= 0; i--) {
			fraseInvertida.append(cortes[i]);
			fraseInvertida.append(" ");
		}
		
		return fraseInvertida.toString();
	}
		
}
