package com.exercicios.aula6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class TestaPerformance {
	
	public static void main(String[] args) {
		
       System.out.println("Iniciando teste de INSERÇÃO em um ArrayList...");
       testaPerformanceInsercaoPorColecao(new ArrayList<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de INSERÇÃO em uma LinkedList...");
       testaPerformanceInsercaoPorColecao(new LinkedList<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de INSERÇÃO em um HashSet...");
       testaPerformanceInsercaoPorColecao(new HashSet<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de PESQUISA em um ArrayList...");
       testaPerformancePesquisaPorColecao(new ArrayList<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de PESQUISA em uma LinkedList...");
       testaPerformancePesquisaPorColecao(new LinkedList<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de PESQUISA em um HashSet...");
       testaPerformancePesquisaPorColecao(new HashSet<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de INSERÇÃO NA PRIMEIRA POSIÇÃO em um ArrayList...");
       testaPerformanceInsercaoNaPrimeiraPosicaoEmArrayList(new ArrayList<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de INSERÇÃO NA PRIMEIRA POSIÇÃO em uma LinkedList...");
       testaPerformanceInsercaoNaPrimeiraPosicaoEmLinkedList(new LinkedList<Integer>());
       
       System.out.print("\n");
       
       System.out.println("Iniciando teste de INDICE em um ArrayList...");
       testaPerformancePorIndiceEmList(new ArrayList<Integer>());
       
       System.out.print("\n");
      
       System.out.println("Iniciando teste de INDICE em um LinkedList...");
       testaPerformancePorIndiceEmList(new LinkedList<Integer>());

       System.out.println("\nFIM!!!!!!!");
   }
	
	public static void testaPerformanceInsercaoPorColecao(Collection<Integer> colecaoTeste) {
		
       long inicio = System.currentTimeMillis();

       int total = 100000;

       for (int i = 0; i < total; i++) {
    	   colecaoTeste.add(i);
       }

       long fim = System.currentTimeMillis();
       long tempo = fim - inicio;
       System.out.println("Tempo gasto: " + tempo);
	}
	
	public static void testaPerformancePesquisaPorColecao(Collection<Integer> colecaoTeste) {
		
	   int total = 100000;
	   
	   for (int i = 0; i < total; i++) {
		   colecaoTeste.add(i);
	   }
	   
	   long inicio = System.currentTimeMillis();
	
	   for (int i = 0; i < total; i++) {
		   colecaoTeste.contains(i);
	   }
	
	   long fim = System.currentTimeMillis();
	   long tempo = fim - inicio;
	   System.out.println("Tempo gasto: " + tempo);
	}
	
	public static void testaPerformanceInsercaoNaPrimeiraPosicaoEmArrayList(ArrayList<Integer> colecaoTeste) {
		
       long inicio = System.currentTimeMillis();

       int total = 100000;

       for (int i = 0; i < total; i++) {
    	   colecaoTeste.add(0, i);
       }

       long fim = System.currentTimeMillis();
       long tempo = fim - inicio;
       System.out.println("Tempo gasto: " + tempo);
	}
	
	public static void testaPerformanceInsercaoNaPrimeiraPosicaoEmLinkedList(LinkedList<Integer> colecaoTeste) {
		
       long inicio = System.currentTimeMillis();

       int total = 100000;

       for (int i = 0; i < total; i++) {
    	   colecaoTeste.add(0, i);
       }

       long fim = System.currentTimeMillis();
       long tempo = fim - inicio;
       System.out.println("Tempo gasto: " + tempo);
	}
	
	public static void testaPerformancePorIndiceEmList(List<Integer> colecaoTeste) {
			
	   int total = 100000;

       for (int i = 0; i < total; i++) {
    	   colecaoTeste.add(0, i);
       }
       
       long inicio = System.currentTimeMillis();

       for (int i = 0; i < total; i++) {
    	   colecaoTeste.get(i);
       }

       long fim = System.currentTimeMillis();
       long tempo = fim - inicio;
       System.out.println("Tempo gasto: " + tempo);
	}
	
}
