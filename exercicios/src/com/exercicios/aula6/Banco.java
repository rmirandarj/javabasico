package com.exercicios.aula6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Banco {
	
	private Map<String, Conta> listaContas = new HashMap<String, Conta>();
	
	public void adicionaConta(Conta c) {
		this.listaContas.put(c.getTitular(), c);
	}
	
	public int quantidadeDeContas() {
		return this.listaContas.size();
	}
	
	public Conta buscaPorTitular(String titular) {
		return this.listaContas.get(titular);
	}
	
	
}
