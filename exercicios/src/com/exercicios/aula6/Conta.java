package com.exercicios.aula6;

import java.util.List;
import java.util.Random;

import com.exercicios.aula4.model.Imprimivel;

public class Conta implements Comparable<Conta>, Imprimivel{
	
	private int agencia;
	private int conta;
	private String titular;
	private double saldo;
	
	public double getSaldo() {
		return saldo;
	}

	public String getTitular() {
		return titular;
	}

	protected Conta(int agencia, int conta, String titular, double saldo) {
		super();
		this.agencia = agencia;
		this.conta = conta;
		this.titular = titular;
		this.saldo = saldo;
	}

	//Delegando para a classe String, pois esta implementa a Comparable e sabe comparar Strings
	@Override
	public int compareTo(Conta outraConta) {
		return this.getTitular().compareTo(outraConta.getTitular());
	}

	@Override
	public String mostrarDados() {
		
		return "Conta: Titular = " + this.titular + ", saldo = " + (this.saldo) + "\n";
	}
	
	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getConta() {
		return conta;
	}

	public void setConta(int conta) {
		this.conta = conta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + agencia;
		result = prime * result + conta;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		if (agencia != other.agencia)
			return false;
		if (conta != other.conta)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Conta [agencia=" + agencia + ", conta=" + conta + ", titular=" + titular + ", saldo=" + saldo + "]";
	}
	
	public static void adicionarContasAleatorias(List<Conta> listaContas, int numContas) {
		
		Random r = new Random();
		for (int i = 0; i < numContas; i++) {
			Conta conta = new Conta(i, r.nextInt(100) * 999, "Titular" + (r.nextInt(100)), r.nextInt(100) * 100);
			listaContas.add(conta);
		}
		
	}
	
	
	
}
