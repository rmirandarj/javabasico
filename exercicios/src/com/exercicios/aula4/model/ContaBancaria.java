package com.exercicios.aula4.model;


public abstract class ContaBancaria implements Imprimivel{
	
	protected int numeroConta;
	protected double saldo;
	
	
	
	public ContaBancaria(int numeroConta, double saldo) {
		super();
		this.numeroConta = numeroConta;
		this.saldo = saldo;
	}
	
	public abstract void sacar(double valor);
	public abstract void depositar(double valor);
	
	public void transferir(double valor, ContaBancaria contaDestino) {
		this.sacar(valor);
		contaDestino.depositar(valor);
	}
}
