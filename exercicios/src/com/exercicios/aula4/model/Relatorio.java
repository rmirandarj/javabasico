package com.exercicios.aula4.model;

public class Relatorio {
	
	public void gerarRelatorio(Imprimivel imprimivel) {
		System.out.println("Relatório\n\n " + imprimivel.mostrarDados());
	}

}
