package com.exercicios.aula4.model;

import java.util.ArrayList;
import java.util.List;

public class Banco implements Imprimivel{
	
	private String nome;
	
	public Banco(String nome) {
		super();
		this.nome = nome;
	}

	private List<ContaBancaria> listaContas = new ArrayList<ContaBancaria>();
	
	public boolean inserirConta(ContaBancaria conta) {
		return listaContas.add(conta);
	}
	
	public boolean removerConta(ContaBancaria conta) {
		return listaContas.remove(conta);
	}
	
	public ContaBancaria procurarConta(int numeroConta) {
		
		for (ContaBancaria contaBancaria : listaContas) {
			if(contaBancaria.numeroConta == numeroConta) return contaBancaria;
		}
		
		return null;
	}

	@Override
	public String mostrarDados() {
		StringBuilder dados = new StringBuilder("\n\tContas do Banco " + this.nome + ":\n\n");
		
		listaContas.forEach(contaBancaria->{
			
			if(contaBancaria != null) {
				dados.append(contaBancaria.mostrarDados());
				dados.append("\n");
			}
		});
		
		return dados.toString();
	}

	public String getNome() {
		return nome;
	}
	
	
}
