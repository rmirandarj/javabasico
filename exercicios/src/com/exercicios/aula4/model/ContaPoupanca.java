package com.exercicios.aula4.model;

import com.exercicios.aula5.exceptions.SaldoInsuficienteException;

public class ContaPoupanca extends ContaBancaria{
	
	private double limite;
	
	public ContaPoupanca(int numeroConta, double saldo, double limite) {
		super(numeroConta, saldo);
		this.limite = limite;
	}

	@Override
	public void sacar(double valor) {
		
		if(valor < 0) {
			throw new IllegalArgumentException("O valor informado para saque é inválido");
		}
		
		if(super.saldo + limite < valor) {
			throw new SaldoInsuficienteException(valor);
		}
		else {
			super.saldo = super.saldo - valor;
		}
	}

	@Override
	public void depositar(double valor) {
		
		if(valor < 0) {
			throw new IllegalArgumentException("O valor informado para depósito é inválido");
		}
		
		super.saldo += valor;
		
	}

	@Override
	public String mostrarDados() {
		return "\t\tConta Poupança: saldo = " + (super.saldo + limite) + "\n";
	}

	
}
