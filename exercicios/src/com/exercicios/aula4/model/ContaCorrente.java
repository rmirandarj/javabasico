package com.exercicios.aula4.model;

import com.exercicios.aula5.exceptions.SaldoInsuficienteException;

public class ContaCorrente extends ContaBancaria{

	private double taxaDeOperacao;

	
	
	public ContaCorrente(int numeroConta, double saldo, double taxaDeOperacao) {
		super(numeroConta, saldo);
		this.taxaDeOperacao = taxaDeOperacao;
	}

	@Override
	public void sacar(double valor) {
		
		if(valor < 0) {
			throw new IllegalArgumentException("Você tentou sacar um valor negativo");
		}
		
		double valorTotalRetirado = valor + taxaDeOperacao;
		
		if(super.saldo < valorTotalRetirado) {
			throw new SaldoInsuficienteException(valor);
		}
		else {
			super.saldo = super.saldo - valorTotalRetirado;
		}
	}

	@Override
	public void depositar(double valor) {
		
		if(valor < 0) {
			throw new IllegalArgumentException("Você tentou depositar um valor negativo");
		}
		
		if(super.saldo + valor < taxaDeOperacao) {
			throw new SaldoInsuficienteException("Você não possui saldo suficiente para efetuar esta operação. "
					+ "O seu saldo somado ao valor de depósito é menor que a taxa de operação que seria descontada.");
		}
		else {
			super.saldo = super.saldo + valor - taxaDeOperacao;
		}
	}
	
	@Override
	public String mostrarDados() {
		return "\t\tConta corrente: saldo = " + super.saldo + ", taxa de operação = " + taxaDeOperacao + "\n";
	}

}
