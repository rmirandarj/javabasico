package com.exercicios.aula1;

public class Exercicios {
	
	//exercício 1
	public void imprimirNumerosEmRange(int inicio, int fim) {
		for(int i = inicio; i <= fim; i++)
			System.out.print(i + " ");
		System.out.print("\n");
	}
	
	//exercício 2
	public void imprimirSomaEmRange(int inicio, int fim) {
		int soma = 0;
		
		for(int i = inicio; i <= fim; i++)
			soma += i;
		
		System.out.println(soma);
	}
	
	//exercício 3
	public void imprimirMultiplosEmRange(int mult, int inicio, int fim) {
		
		for(int i = inicio; i <= fim; i++)		
			System.out.print(i * mult + " ");
		
		System.out.print("\n");
	}
	
	//exercicio 4
	public void imprimirFatorial(int numero) {
		 for (int n = 1; n <= numero; n++) {
			 long resultado = 1;
			 for (int i = 1; i < n; i++) {
				 resultado *= i;
				 System.out.println("O fatorial de " + i + " é: " + resultado);
			 }
			 System.out.print("\n");
		 }
	}
	
	
	
	//exercicio 5 e desafio
	public void imprimirFibonacciComLimite(int limite) {
		
		StringBuffer dados = new StringBuffer(0 + ", " + 1 + ", ");
		
		for(int a = 0, b = 1; a+b <= limite; b += a,  a = b - a)
			dados.append(a+b + ", ");
		
		System.out.println(dados.delete(dados.length()-2, dados.length()-1));
	}
	
	
	//exercicio 7
	public void imprimirMatrizQuadradaTriagularInferior(int tamanho) {
		for(int i = 1; i < tamanho; i++) {
			System.out.print("\n");
			for(int j = 1; j <= i; j++) {
				System.out.print(i * j + " ");
			}
		}
	}	
	
}
