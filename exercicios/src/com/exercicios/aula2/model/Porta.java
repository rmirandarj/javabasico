package com.exercicios.aula2.model;

public class Porta {
	private boolean aberta = false;
	private String cor;
	private double dimensaoX;
	private double dimensaoY;
	private double dimensaoZ;
	
	
	
	public Porta(String cor, double dimensaoX, double dimensaoY, double dimensaoZ) {
		super();
		this.cor = cor;
		this.dimensaoX = dimensaoX;
		this.dimensaoY = dimensaoY;
		this.dimensaoZ = dimensaoZ;
	}

	public boolean isAberta() {
		return aberta;
	}

	public String getCor() {
		return cor;
	}

	public double getDimensaoX() {
		return dimensaoX;
	}

	public double getDimensaoY() {
		return dimensaoY;
	}

	public double getDimensaoZ() {
		return dimensaoZ;
	}

	public void abre() {
		this.aberta = true;
	}
	
	public void fecha() {
		this.aberta = false;
	}
	
	public void pinta(String cor) {
		this.cor = cor;
	}
	
	public boolean estaAberta() {
		return this.isAberta();
	}
	
}
