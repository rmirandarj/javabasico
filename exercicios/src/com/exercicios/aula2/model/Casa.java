package com.exercicios.aula2.model;

import java.util.ArrayList;

public class Casa {
	
	private String cor;
	private ArrayList<Porta> portas = new ArrayList<Porta>();
	
	public String getCor() {
		return cor;
	}

	public ArrayList<Porta> getPortas() {
		return portas;
	}

	public void pinta(String cor) {
		this.cor = cor;
	}
	
	public void addPorta(Porta porta) {
		this.portas.add(porta);
	}
	
	public Casa(String cor, Porta p1, Porta p2, Porta p3) {
		this.cor = cor;
		this.portas.add(p1);
		this.portas.add(p2);
		this.portas.add(p3);
	}
	
	public int getQuantasPortasEstaoAbertas() {
		int count = 0;
		
		for (Porta porta : this.portas) {
			if(porta.isAberta())	count++;
		}
		
		return count;
	}
}
