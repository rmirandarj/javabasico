package com.exercicios.aula2.model;

import java.time.LocalDate;

public class Data {
	
	private int dia;
	private int mes;
	private int ano;
	
	public int getDia() {
		return dia;
	}

	public int getMes() {
		return mes;
	}

	public int getAno() {
		return ano;
	}
	
	@Override
	public String toString() {
		return this.getDia() +"/" + this.getMes() + "/" + this.getAno();
	}

	private Data(int dia, int mes, int ano) {
		super();
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}

	public static Data now() {
		LocalDate date = LocalDate.now();
		return new Data(date.getDayOfMonth(), date.getMonthValue(), date.getYear());
	}
	
}
