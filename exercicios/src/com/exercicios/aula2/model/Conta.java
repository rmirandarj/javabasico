package com.exercicios.aula2.model;

public class Conta {
	
	private static int sequence = 1;
	
	private int id;
	private String nomeTitular;
	private int numero;
	private String agencia;
	private double saldo;
	private Data dataAbertura;
	
	public Conta(String nomeTitular, int numero, String agencia, double saldo) {
		super();
		this.id = sequence++;
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.agencia = agencia;
		this.saldo = saldo;
		this.dataAbertura = Data.now();
	}
	
	public Conta(int numero, String agencia, double saldo) {
		super();
		this.id = sequence++;
		this.numero = numero;
		this.agencia = agencia;
		this.saldo = saldo;
		this.dataAbertura = Data.now();;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public int getNumero() {
		return numero;
	}

	public String getAgencia() {
		return agencia;
	}

	public double getSaldo() {
		return saldo;
	}

	public Data getDataAbertura() {
		return dataAbertura;
	}

	public void saca(double valor) {
        this.saldo -= valor; 
    }

	public void deposita(double valor) {
        this.saldo += valor;
    }

	public double calculaRendimento() {
        return this.saldo * 0.1;
    }
	
	public String recuperaDadosParaImpressao() {
		StringBuffer dados = new StringBuffer();
		
		dados.append("\nId: " + this.getId());
		dados.append("\nTitular: " + this.getNomeTitular());
		dados.append("\nNumero: " + this.getNumero());
		dados.append("\nAgência: " + this.getAgencia());
		dados.append("\nSaldo: " + this.getSaldo());
		dados.append("\nData: " + this.getDataAbertura());
		
		return dados.toString();
	}

	public int getId() {
		return id;
	}
}
