package com.exercicios.aula2.model;

public class FabricaDeCarro {
	
	private static FabricaDeCarro instance = null; 
	private String modelo;
	
	
    
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	private FabricaDeCarro(String modelo){
		this.modelo = modelo;
	}  
	
    public static FabricaDeCarro getInstance(){  
        if (instance == null){  
            instance = new FabricaDeCarro("Modelo único");  
        }  
        return instance;  
    }
    
    
    
}
