package com.exercicios.aula2.model;

public class Pessoa {
	
	private String nome;
	private int idade;
	
	public Pessoa(String nome, int idade) {
		super();
		this.nome = nome;
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public int getIdade() {
		return idade;
	}
	
	public void fazerAniversario() {
		this.idade++;
	}
	
	@Override
	public String toString() {
		return "Nome: " + this.getNome() + "\nIdade: " + this.getIdade();
	}
}
