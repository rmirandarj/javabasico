package com.exercicios.aula3.model;

public class DiaDosNamorados extends CartaoWeb{

	public DiaDosNamorados(String destinatario) {
		super(destinatario);
	}

	@Override
	public String showMessage() {
		return "Feliz dia dos namorados " + super.destinatario +"!!!";
	}

}
