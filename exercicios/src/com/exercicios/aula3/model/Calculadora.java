package com.exercicios.aula3.model;

public class Calculadora {
	
	public double somar(double n1, double n2) {
		return new Soma().calcular(n1, n2);
	}
	
	public double subtrair(double n1, double n2) {
		return new Subtracao().calcular(n1, n2);
	}
	
	public double multiplicar(double n1, double n2) {
		return new Multiplicacao().calcular(n1, n2);
	}
	
	public double dividir(double n1, double n2) {
		return new Divisao().calcular(n1, n2);
	}
}
