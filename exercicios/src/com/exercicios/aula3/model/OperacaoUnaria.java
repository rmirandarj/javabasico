package com.exercicios.aula3.model;

public abstract class OperacaoUnaria {
	
	protected abstract double calcular(double n);

}
