package com.exercicios.aula3.model;

public class CalculadoraCientifica extends Calculadora {
	
	public double pontenciacao(double n1, double n2) {
		return new Potencia().calcular(n1, n2);
	}
	
	public double raizQuadrada(double n) {
		return new RaizQuadrada().calcular(n);
	}
	
	
}
