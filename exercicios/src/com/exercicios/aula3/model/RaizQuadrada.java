package com.exercicios.aula3.model;

public class RaizQuadrada extends OperacaoUnaria{

	@Override
	public double calcular(double n) {
		return Math.sqrt(n);
	}

}
