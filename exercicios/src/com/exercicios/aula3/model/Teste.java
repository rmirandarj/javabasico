package com.exercicios.aula3.model;

public class Teste {
	
	public static void main(String[] args) {
		
		CalculadoraCientifica calculadora = new CalculadoraCientifica();
		System.out.println(calculadora.dividir(5, 2));
		System.out.println(calculadora.pontenciacao(2.0, 5));
		
		System.out.print("\n");
		
		Funcionario gerente = new Gerente("Gerente", "1", 1000);
		Funcionario assistente = new Assistente("Assistente", "2", 1000);
		Funcionario vendedor = new Vendedor("Vendedor", "3", 1000, 300);
		
		imprimeSalarioDoFuncionario(gerente);
		imprimeSalarioDoFuncionario(assistente);
		imprimeSalarioDoFuncionario(vendedor);
		
		System.out.print("\n");
		
		CartaoWeb[] cartoes = new CartaoWeb[3];
		
		cartoes[0] = new DiaDosNamorados("Fulano");
		cartoes[1] = new Natal("Cicrano");
		cartoes[2] = new Aniversario("Beltrano");

		for (CartaoWeb cartaoWeb : cartoes) {
			System.out.println(cartaoWeb.showMessage());
		}
		
	}
	
	public static void imprimeSalarioDoFuncionario(Funcionario funcionario) {
		System.out.println(funcionario.calculaSalario());
	}
}
