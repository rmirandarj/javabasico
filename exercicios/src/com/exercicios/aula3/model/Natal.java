package com.exercicios.aula3.model;

public class Natal extends CartaoWeb{

	public Natal(String destinatario) {
		super(destinatario);
	}

	@Override
	public String showMessage() {
		return "Feliz natal " + super.destinatario +"!!!";
	}

}
