package com.exercicios.aula3.model;

public abstract class OperacaoBinaria {
	
	protected abstract double calcular(double n1, double n2);
	
}
