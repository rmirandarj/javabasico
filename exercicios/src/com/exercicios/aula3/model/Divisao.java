package com.exercicios.aula3.model;

public class Divisao extends OperacaoBinaria{

	@Override
	public double calcular(double n1, double n2) {
		return n1 / n2;
	}
	
	
}
