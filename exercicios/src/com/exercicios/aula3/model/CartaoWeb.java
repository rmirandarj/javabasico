package com.exercicios.aula3.model;

public abstract class CartaoWeb {
	
	protected String destinatario;
	
	protected CartaoWeb(String destinatario) {
		super();
		this.destinatario = destinatario;
	}

	public abstract String showMessage();
}
