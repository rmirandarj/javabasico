package com.exercicios.aula3.model;

public class Potencia extends OperacaoBinaria{

	@Override
	public double calcular(double n1, double n2) {
		return Math.pow(n1, n2);
	}

}
