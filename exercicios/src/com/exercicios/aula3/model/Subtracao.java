package com.exercicios.aula3.model;

public class Subtracao extends OperacaoBinaria{

	@Override
	public double calcular(double n1, double n2) {
		return n1 - n2;
	}

}
