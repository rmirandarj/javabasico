package com.exercicios.aula3.model;

public abstract class Funcionario {
	
	protected String nome;
	protected String matricula;
	protected double salarioBase;
	
	protected Funcionario(String nome, String matricula, double salarioBase) {
		super();
		this.nome = nome;
		this.matricula = matricula;
		this.salarioBase = salarioBase;
	}



	public abstract double calculaSalario();
}
