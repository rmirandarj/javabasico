import com.exercicios.aula1.Exercicios;
import com.exercicios.aula2.model.Casa;
import com.exercicios.aula2.model.Conta;
import com.exercicios.aula2.model.FabricaDeCarro;
import com.exercicios.aula2.model.Fibonacci;
import com.exercicios.aula2.model.Pessoa;
import com.exercicios.aula2.model.Porta;
import com.exercicios.aula3.model.Aniversario;
import com.exercicios.aula3.model.Assistente;
import com.exercicios.aula3.model.CalculadoraCientifica;
import com.exercicios.aula3.model.CartaoWeb;
import com.exercicios.aula3.model.DiaDosNamorados;
import com.exercicios.aula3.model.Funcionario;
import com.exercicios.aula3.model.Gerente;
import com.exercicios.aula3.model.Natal;
import com.exercicios.aula3.model.Vendedor;
import com.exercicios.aula4.model.Banco;
import com.exercicios.aula4.model.ContaBancaria;
import com.exercicios.aula4.model.ContaCorrente;
import com.exercicios.aula4.model.ContaPoupanca;
import com.exercicios.aula4.model.Imprimivel;
import com.exercicios.aula4.model.Relatorio;

public class Principal {
	
	
public static void main(String[] args) {
		
		//Aula 1
		Exercicios exercicios = new Exercicios();
		
		exercicios.imprimirNumerosEmRange(150, 300);
		exercicios.imprimirSomaEmRange(1, 1000);
		System.out.print("\n");
		exercicios.imprimirMultiplosEmRange(3, 1, 100);
		exercicios.imprimirFatorial(15);
		exercicios.imprimirFibonacciComLimite(100);
		exercicios.imprimirMatrizQuadradaTriagularInferior(10);
		
		System.out.print("\n");
//-----------------------------------------------------------------------------------------------------------------------------------		
		
		//Aula 2
        Conta c1 = new Conta("Hugo", 123, "45678-9", 50.0);
        Conta c2 = new Conta("Hugo2", 123, "45678-9", 50.0);

        c1.deposita(100.0);
        
        System.out.println(c1.recuperaDadosParaImpressao());
        System.out.println(c2.recuperaDadosParaImpressao());
        
        System.out.print("\n");
		
		
		Fibonacci fibonacci = new Fibonacci();
		
		System.out.println(fibonacci.calculaFibonacci(6));
		
		System.out.print("\n");
		
		
		Pessoa pessoa = new Pessoa("Raphael", 24);
		System.out.println(pessoa);
		
		System.out.print("\n");
		
		pessoa.fazerAniversario();
		System.out.println(pessoa);
		
		System.out.print("\n");

		
		
		Porta p1 = new Porta("Preta", 50, 200, 5);
		Porta p2 = new Porta("Preta", 50, 200, 5);
		Porta p3 = new Porta("Preta", 50, 200, 5);
		
		Casa casa = new Casa("Azul", p1, p2, p3);
		
		p1.abre();
		p2.abre();
		p2.fecha();
		
		System.out.println(casa.getQuantasPortasEstaoAbertas());
        
		//exemplo de Sigleton
		FabricaDeCarro fabricaDeCarro = FabricaDeCarro.getInstance();
		System.out.println(fabricaDeCarro.getModelo());
		  
		//alterando a única referência em memória
		fabricaDeCarro.setModelo("Modelo alterado");
		  
		FabricaDeCarro fabricaDeCarro2 = FabricaDeCarro.getInstance();
		System.out.println(fabricaDeCarro2.getModelo());
		
		System.out.print("\n");
		
//-----------------------------------------------------------------------------------------------------------------------------------		
		
		
		//Aula 3
		CalculadoraCientifica calculadora = new CalculadoraCientifica();
		System.out.println(calculadora.dividir(5, 2));
		System.out.println(calculadora.pontenciacao(2.0, 5));
		
		System.out.print("\n");
		
		Funcionario gerente = new Gerente("Gerente", "1", 1000);
		Funcionario assistente = new Assistente("Assistente", "2", 1000);
		Funcionario vendedor = new Vendedor("Vendedor", "3", 1000, 300);
		
		imprimeSalarioDoFuncionario(gerente);
		imprimeSalarioDoFuncionario(assistente);
		imprimeSalarioDoFuncionario(vendedor);
		
		System.out.print("\n");
		
		CartaoWeb[] cartoes = new CartaoWeb[3];
		
		cartoes[0] = new DiaDosNamorados("Fulano");
		cartoes[1] = new Natal("Cicrano");
		cartoes[2] = new Aniversario("Beltrano");

		for (CartaoWeb cartaoWeb : cartoes) {
			System.out.println(cartaoWeb.showMessage());
		}
		
		System.out.print("\n");
		
//-----------------------------------------------------------------------------------------------------------------------------------
//		Aulas 4 e 5
		
		Banco banco = new Banco("Bradesco Sede");
		
		ContaBancaria corrente = new ContaCorrente(1, 100, 20);
		ContaBancaria poupanca = new ContaPoupanca(2, 100, 100);
		
		banco.inserirConta(null);
		banco.inserirConta(poupanca);
		
		Relatorio relatorio = new Relatorio();
	
		corrente.transferir(80, poupanca);
		
		corrente.sacar(500);
		relatorio.gerarRelatorio(corrente);
		
		System.out.print("\n");
		
//		poupanca.sacar(150);

		relatorio.gerarRelatorio(poupanca);
		
		banco.removerConta(corrente);
		
		relatorio.gerarRelatorio(banco);
		
		

	}


	public static void imprimeSalarioDoFuncionario(Funcionario funcionario) {
		System.out.println(funcionario.calculaSalario());
	}
}
